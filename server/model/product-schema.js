const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
  id: String,
  robotType: Number,
  photo: String,
  name: String,
  cname: String,
  range: [Number],
  guide: [{
      id: String,
      name: String,
      info: [String]
  }],
  paymentPlan: {
      label: String,
      name: String,
      onChange: String,
      selectList: [String],
      value: Buffer,
      selectIndex: Number
  },
  monthlyFees: Number,
  seriesCode: String,
  status: String
});

const Product = module.exports = mongoose.model('Product', productSchema);

module.exports.getProducts = function(req, res) {
  Product.find({}, {
    "id": 1, "name": 1
  }).then(
    resp => {
      res.json(resp);
    }
  ).catch(
    err => {
      console.log(err);
    }
  );
}

module.exports.getProduct = function(req, res) {
  Product.findById(req.params._id).then(
    resp => {
      res.json(resp);
    }
  ).catch(
    err => {
      console.log(err);
    }
  );
}

module.exports.addProduct = function(req, res) {
  let newProduct = req.body;
  Product.create(newProduct).then(
    resp => {
      res.json(resp);
    }
  ).catch(
    err => {
      console.log(err);
    }
  );
}

module.exports.updateProduct = function(req, res) {
  let query = {_id: req.params._id};
  let newProduct = req.body;
  Product.findOneAndUpdate(query, newProduct).then(
    resp => {
      res.json(resp);
    }
  ).catch(
    err => {
      console.log(err);
    }
  );
}

module.exports.deleteProduct = function(req, res) {
  let query = {_id: req.params._id};
  Product.remove(query).then(
    resp => {
      res.json(resp);
    }
  ).catch(
    err => {
      console.log(err);
    }
  );
}
