// server/api.js
/*
 |--------------------------------------
 | Dependencies
 |--------------------------------------
 */
// import model
const Product = require('./model/product-schema');

/*
 |--------------------------------------
 | Authentication Middleware
 |--------------------------------------
 */
module.exports = function(app, config) {

    /*
     |--------------------------------------
     | API Routes
     |--------------------------------------
     */

    // GET API root
    app.get('/api/', (req, res) => {
        res.send('API works');
    });

    // products api
    app.get('/api/products', function(req, res) { Product.getProducts(req, res); });
    app.get('/api/products/:_id', function(req, res) { Product.getProduct(req, res); });
    app.post('/api/products', function(req, res) { Product.addProduct(req, res); });
    app.put('/api/products/:_id', function(req, res) { Product.updateProduct(req, res); });
    app.delete('/api/products/:_id', function(req, res) { Product.deleteProduct(req, res); });

};