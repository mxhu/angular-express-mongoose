/*
 |--------------------------------------
 | Dependencies
 |--------------------------------------
 */
// Modules
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const methodOverride = require('method-override');
const cors = require('cors');
const path = require('path');

//configuration
const config = require('./config');

/*
|--------------------------------------
| MongoDB
|--------------------------------------
*/
// Connect to db
mongoose.Promise = require('bluebird');
mongoose.connect(config.dbConnection);
const db = mongoose.connection;
db.on('err', function() {
    console.error('MongoDB Connection Error. Please make sure that', config.dbConnection, 'is running.');
});
db.once('open', function() {
    console.log("connected to db");
});

/*
 |--------------------------------------
 | App
 |--------------------------------------
 */
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(cors());
// Set port
const port = process.env.PORT || '3000';
app.set('port', port);

// Serve static silent.html file at /silent
app.use('/silent', express.static(path.join(__dirname, './silent.html')));

// Set static path to Angular app in dist
// Don't run in dev
if (process.env.NODE_ENV !== 'dev') {
    app.use('/', express.static(path.join(__dirname, './dist')));
}

/*
 |--------------------------------------
 | Routes
 |--------------------------------------
 */
require('./server/api')(app, config);
// Pass routing to Angular app
// Don't run in dev
if (process.env.NODE_ENV !== 'dev') {
    app.get('*', function(req, res) {
        res.sendFile(path.join(__dirname, '/dist/index.html'));
    });
}

/*
 |--------------------------------------
 | Server
 |--------------------------------------
 */
app.listen(port, () => console.log(`Server running on localhost:${port}`));