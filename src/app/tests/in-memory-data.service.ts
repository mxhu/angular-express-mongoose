import { InMemoryDbService } from 'angular-in-memory-web-api';
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const products = [
      {
        _id: "5940f36ef36d2813bffc183e",
        id: "5940f36ef36d2813bffc183e",
        robotType : 1,
        photo: "https://res.cloudinary.com/drdjwgwdq/image/upload/home/autogain-1-v1.jpg",
        name: "R01: Autogain Upgrade NO.1(30% STOP LOSS)",
        cname: "R01: Autogain Upgrade 壹号 (30%止损)",
        range: [ 10000, 20000],
        guide: [
		             {
                   id: "01",
                   name: "RECOMMENDED FOR TRADING ACCOUNT WITH INITIAL DEPOSIT OF:",
                   info: [ "USD 10,000 – USD 20,000" ]
                 },
                 {
                   id: "02",
                   name: "建议账户初始金额：",
                   info: [ "美金 10,000 – 20,000", "美金 10,000 – 20,999" ]
                 }
               ],
        paymentPlan: {
                      label: "价格及支付方案",
                      name: "paymentPlan",
                      onChange: "changePaymentPlan",
                      selectList: [ "每月支付人民币 ¥2380" ],
                      value : null,
                      selectIndex: -1
                    },
        monthlyFees: 445.5,
        seriesCode: "AutoGain",
        status: "1"
      },
      {
        _id: "5940f36ef36d2813bffc1fff",
        id: "5940f36ef36d2813bffc1fff",
        robotType : 1,
        photo: "https://res.cloudinary.com/drdjwgwdq/image/upload/home/autogain-1-v1.jpg",
        name: "R01: Autogain Upgrade NO.2(30% STOP LOSS)",
        cname: "R01: Autogain Upgrade 2号 (30%止损)",
        range: [ 10000, 20000],
        guide: [
		             {
                   id: "01",
                   name: "RECOMMENDED FOR TRADING ACCOUNT WITH INITIAL DEPOSIT OF:",
                   info: [ "USD 10,000 – USD 20,000" ]
                 },
                 {
                   id: "02",
                   name: "建议账户初始金额：",
                   info: [ "美金 10,000 – 20,000" ]
                 }
               ],
        paymentPlan: {
                      label: "价格及支付方案",
                      name: "paymentPlan",
                      onChange: "changePaymentPlan",
                      selectList: [ "每月支付人民币 ¥2380" ],
                      value : null,
                      selectIndex: -1
                    },
        monthlyFees: 445.5,
        seriesCode: "AutoGain",
        status: "1"
      }
    ];
    return {products};
  }
}