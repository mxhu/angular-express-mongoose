export class Product {
  _id: string;
  id: string;
  robotType: number;
  photo: string;
  name: string;
  cname: string;
  range: number[];
  guide: Guide[];
  paymentPlan: PaymentPlan;
  monthlyFees: number;
  seriesCode: string;
  status: string;
}

export class Guide {
    id: string;
    name: string;
    info: string[];
}

export class PaymentPlan {
    label: string;
    name: string;
    onChange: string;
    selectList: string[];
    value: any;
    selectIndex: number;
}