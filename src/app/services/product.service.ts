import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Product } from '../models/product';

@Injectable()
export class ProductService {

  private heasers = new Headers({
    'Content-Type': 'application/json'
  });
  private productsUrl = 'http://localhost:3000/api/products';

  constructor(private http: Http) { }

  getProducts(): Promise<Product[]> {
    return this.http
      .get(this.productsUrl)
      .toPromise()
      .then(res => res.json() as Product[])
      .catch(this.handleError);
  }

  getProduct(_id: string): Promise<Product> {
    const url = `${this.productsUrl}/${_id}`;
    return this.http
      .get(url)
      .toPromise()
      .then(res => res.json() as Product)
      .catch(this.handleError);
  }

  delete(_id: string): Promise<void> {
    const url = `${this.productsUrl}/${_id}`;
    return this.http
      .delete(url, {headers: this.heasers})
      .toPromise()
      .then()
      .catch(this.handleError);
  }

  create(newProduct: Product): Promise<Product> {
    return this.http
      .post(this.productsUrl, JSON.stringify({
        id: newProduct.id,
        robotType: newProduct.robotType,
        photo: newProduct.photo,
        name: newProduct.name,
        cname: newProduct.cname,
        range: newProduct.range,
        guide: newProduct.guide,
        paymentPlan: newProduct.paymentPlan,
        monthlyFees: newProduct.monthlyFees,
        seriesCode: newProduct.seriesCode,
        status: newProduct.status
      }), {headers: this.heasers})
      .toPromise()
      .then()
      .catch(this.handleError);
  }

  update(newProduct: Product): Promise<Product> {
    const url = `${this.productsUrl}/${newProduct._id}`;
    return this.http
      .put(url, JSON.stringify(newProduct), {headers: this.heasers})
      .toPromise()
      .then()
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // only for demo
    return Promise.reject(error.message || error);
  }
}
