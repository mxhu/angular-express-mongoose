import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './tests/in-memory-data.service';

import { AppComponent } from './app.component';
import { ProductsComponent } from './pages/products/products.component';
import { ProductService } from './services/product.service';
import { ProductDetailComponent } from './pages/product-detail/product-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ProductDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    //InMemoryWebApiModule.forRoot(InMemoryDataService)
  ],
  providers: [ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
