import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';

import 'rxjs/add/operator/switchMap';

import { Product, Guide, PaymentPlan } from '../../models/product';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  flag: string;
  product: Product;
  productForm: FormGroup;

  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder
  ) {
    this.productForm = this.fb.group({
      _id: '',
      id: '',
      robotType: 0,
      photo: '',
      name: '',
      cname: '',
      range: this.fb.array([this.fb.control(0)]),
      guide: this.fb.array([this.fb.group({
        id: '',
        name: '',
        info: this.fb.array([this.fb.control('')])
      })]),
      paymentPlan: this.fb.group({
        label: '',
        name: '',
        onChange: '',
        selectList: this.fb.array([this.fb.control('')]),
        value: null,
        selectIndex: 0
      }),
      monthlyFees: 0,
      seriesCode: '',
      status: ''
    });
  }

  ngOnInit(): void {
    this.route.paramMap
      .switchMap((params: ParamMap) => this.flag = params.get('_id'))
      .subscribe(() => {
        if (this.flag === 'new') {
          this.product = new Product();
        } else {
          this.productService.getProduct(this.flag).then(
            product => {
              this.product = product;
              this.setFormValue();
            }
          ).catch(
            err => console.log(err)
            );
        }
      });
  }

  setFormValue(): void {
    this.productForm.reset({
      _id: this.product._id,
      id: this.product.id,
      robotType: this.product.robotType,
      photo: this.product.photo,
      name: this.product.name,
      cname: this.product.cname,
      monthlyFees: this.product.monthlyFees,
      seriesCode: this.product.seriesCode,
      status: this.product.status
    });
    // range
    this.productForm.setControl('range', this.fb.array(this.product.range.map(
      range => this.fb.control(range)
    )));
    // guide
    this.productForm.setControl('guide', this.fb.array(this.product.guide.map(
      guide => this.fb.group({
        id: guide.id,
        name: guide.name,
        info: this.fb.array(guide.info.map(info => this.fb.control(info)))
      })
    )));
    // paymentPlan
    this.productForm.setControl('paymentPlan', this.fb.group({
      label: this.product.paymentPlan.label,
      name: this.product.paymentPlan.name,
      onChange: this.product.paymentPlan.onChange,
      selectList: this.fb.array(this.product.paymentPlan.selectList.map(
        selectList => this.fb.control(selectList)
      )),
      value: this.product.paymentPlan.value,
      selectIndex: this.product.paymentPlan.selectIndex
    }));
  }

  // range form
  get range(): FormArray {
    return this.productForm.get('range') as FormArray;
  }

  removeRange(): void {
    this.range.removeAt(this.range.length - 1);
  }

  addRange(): void {
    this.range.push(this.fb.control(0));
  }

  // guide form
  get guide(): FormArray {
    return this.productForm.get('guide') as FormArray;
  }

  removeGuide(): void {
    this.guide.removeAt(this.guide.length - 1);
  }

  addGuide(): void {
    this.guide.push(this.fb.group({
      id: '',
      name: '',
      info: this.fb.array([''])
    }));
  }

  // info form
  removeInfo(idx): void {
    let guideFG = this.guide.at(idx) as FormGroup;
    let infoFA = guideFG.get('info') as FormArray;
    infoFA.removeAt(infoFA.length - 1);
  }

  addInfo(idx): void {
    let guideFG = this.guide.at(idx) as FormGroup;
    let infoFA = guideFG.get('info') as FormArray;
    infoFA.push(this.fb.control(''));
  }

  // selectList form
  get selectList(): FormArray {
    return this.productForm.get('paymentPlan').get('selectList') as FormArray;
  }

  removeSelectList(): void {
    let selectListFA = this.productForm.get('paymentPlan').get('selectList') as FormArray;
    selectListFA.removeAt(selectListFA.length - 1);
  }

  addSelectList(): void {
    let selectListFA = this.productForm.get('paymentPlan').get('selectList') as FormArray;
    selectListFA.push(this.fb.control(''));
  }

  onSubmit(): void {
    this.product = this.productForm.value;
    if (this.flag == 'new') {
      this.productService
        .create(this.product)
        .then(() => this.goBack());
    } else {
      this.productService
        .update(this.product)
        .then(() => this.goBack());
    }
  }

  reset(): void {
    this.setFormValue();
  }

  goBack(): void {
    this.location.back();
  }
}
