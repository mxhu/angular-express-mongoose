import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Product } from '../../models/product';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products: Product[];
  selectedProduct: Product;

  constructor(
    private productService: ProductService,
    private router: Router) { }

  delete(product: Product): void {
    this.productService
        .delete(product._id)
        .then(() => {
          this.products = this.products.filter(p => p !== product);
          if (this.selectedProduct === product) { this.selectedProduct = null; }
        });
  }

  ngOnInit(): void {
    this.productService
    .getProducts()
    .then(products => {
      this.products = products;
      console.log(this.products)
    });
  }

  onSelect(product: Product): void {
    this.selectedProduct = product;
  }

  gotoDetail(flag: string): void {
    if (flag == 'selected') {
      this.router.navigate(['/detail', this.selectedProduct._id]);
    } else {
      this.router.navigate(['/detail', 'new']);
    }
  }
}
